#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import cherrypy

from registry import *

if __name__ == '__main__':
    class Website(object):

        @cherrypy.expose
        def index(*a, **kw):
            c = u''
            for person in pool():
                c += render_html_of(person)
            return c

    path = os.path.abspath(os.path.dirname(__file__))

    config = {
        "/assets": {
            "tools.staticdir.on": True,
            "tools.staticdir.dir": os.path.abspath(path + '/assets')
        }
    }

    if hasattr(os, 'fork'):
        from cherrypy.process.plugins import Daemonizer
        Daemonizer(cherrypy.engine).subscribe()

    cherrypy.config.update({'server.socket_host': '127.0.0.1',
                            'server.socket_port': 9094,
                            'server.thread_pool': 5})
    cherrypy.quickstart(Website(), config=config)
