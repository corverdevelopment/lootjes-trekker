#!/usr/bin/env python
# -*- coding: utf-8 -*-

from envelopes import Envelope, GMailSMTP
from registry import *


def mail(person):
    envelope = Envelope(from_addr=(u'sinterklaas@corver.it', u'Sinterklaas'),
                        to_addr=person['to'],
                        subject='Sinterklaas Lootje',
                        text_body=render_text_of(person),
                        html_body=render_html_of(person))
    envelope.send('localhost')

for person in pool():
    mail(person)
