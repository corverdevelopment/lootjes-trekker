#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json
import random
from copy import deepcopy

from jinja2 import Environment, FileSystemLoader
from pynliner import Pynliner


path = os.path.abspath(os.path.dirname(__file__))
env = Environment(loader=FileSystemLoader(path))

with open(path + '/data.json') as handle:
    data = json.load(handle)

context = {
    'mail': {
        'subject': 'Sinterklaas Lootjes!'
    }
}


def pool():
    def run():
        r = []
        gone = []
        for person in data:
            p = deepcopy(person)
            r.append(p)
            g = gone + [p['to'][1]] + p['not']
            t = random.choice([x for x in data if x['to'][1] not in g])
            p['ticket'] = deepcopy(t)
            gone.append(t['to'][1])
        return r
    breakout = 10
    while breakout > 0:
        try:
            return run()
        except IndexError as ie:
            breakout -= 1
    raise ValueError('Could not determine pool :-(.')


def render_html_of(person, inline_css=True):
    ctx = dict(person.items() + context.items())
    html = env.get_template('template.html').render(ctx)

    if inline_css:
        p = Pynliner()
        p.from_string(html)
        return p.run()
    else:
        return html


def render_text_of(person, inline_css=True):
    ctx = dict(person.items() + context.items())
    return env.get_template('template.txt').render(ctx)
